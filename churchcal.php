<?php 

/**
Function sorts the ChurchTools-Array by Starttime of the Event.
*/
if ( ! function_exists( 'ctsync_msort' ) ) {
	function ctsync_msort($array){
		$dates = array();
		foreach ($array as $key => $row) {
			$dates[$key]  = strtotime($row->startdate);
		}
		array_multisort($dates, SORT_ASC, $array);
		return $array;
	}
}

if ( ! function_exists( 'ctsync_getUpdatedCalendarEvents' ) ) {
    function ctsync_getUpdatedCalendarEvents(){
        /**
           Function gets newest CalendarEvents from ChurchTools and saves them to Transient. 
           Returns as Array
        */
        $options =  get_option('ctsync_options');
        if(empty($options) || empty($options['url'])){
            return;
		}
        //define options for request
        $data = array('func' => 'getCalendarEvents', 
                      'category_ids' => $options['ids'],
                      'from' => 0,  
                      'to' => $options['maximport']); 
		$result = ctsync_sendRequest($options['url'], $options['userid'], $options['logintoken'], $data);
		if ($result->status == "fail") {
			error_log('ChurchTools Error:'.$result->data);
			echo $result->data;
			return;
		}
        //save data
		if(!empty($result)){ 
			$data=ctsync_msort($result->data);
			set_transient('churchtools_calendar',$data, 24*HOUR_IN_SECONDS);
			set_transient('churchtools_calendar_lastupdated',current_time( 'mysql' ), 24*HOUR_IN_SECONDS);
			return $data;
		}else{
			return null;
		}
	}
}
if ( ! function_exists( 'ctsync_getCalendarEvents' ) ) {
	/**
	Function returns CalendarEvents as an Array
	*/
	function ctsync_getCalendarEvents(){
		if(false === ( $result = get_transient('churchtools_calendar') ) ){
			$result = ctsync_getUpdatedCalendarEvents();
		}
		if(!empty($result)){
			$result = ctsync_msort($result);
		}
		return $result;
	}
}

/**
*Function printCalendarEvents:
*	Mögliche Interessante Daten zur Ausgabe befinden sich in
* 	$var->bezeichnung, $var->startdate, $var->enddate,
*	$var->link,$var->ort, $var->notizen
*/
if(! function_exists('ctsync_sanitize_shortcode')) {
    function ctsync_sanitize_shortcode($atts) {
        // Original Attributes, for filters
        $original_atts = $atts;
        // Pull in shortcode attributes and set defaults
        $atts = shortcode_atts( array(
            //attributes for calendar-shortcode
            'events_per_page'      => get_option('ctsync_options')['maximport'],
            'style'                => 'default',
            'show_update_info'     => false,
            //attributes for both
            'show_category'        => false,
            'offset'               => 0,
            'categories'           => '',
            'title_matches'        => '',
            //attributes for event-shortcode
            'component'            => 'event', //title, description, link, startdate, enddate, date, event
            'modal_id'             => 'churchtools_event_popup',
        ), $atts, 'churchtools_cal' );

        $categories            = sanitize_text_field( $atts['categories'] );
        $atts['offset']        = intval( $atts['offset'] );
        $atts['order']         = sanitize_key( $atts['order'] );
        $atts['orderby']       = sanitize_key( $atts['orderby'] );
        $atts['events_per_page']= intval($atts['events_per_page']);
        $atts['show_category'] = filter_var($atts['show_category'], FILTER_VALIDATE_BOOLEAN);
        $atts['show_update_info'] = filter_var($atts['show_update_info'], FILTER_VALIDATE_BOOLEAN);
        $atts['categories']=[];
        foreach(preg_split('/\D/',$categories) as $id){
            if(intval($id)>0){
                $atts['categories'][] = intval($id);
            }
        }
        if(empty($atts['categories'])){
            $option =  get_option('ctsync_options');
            $atts['categories']=$option['ids'];
        }
        return $atts;
    }
}

if(!function_exists('ctsync_printEventStartTime')) {
    function ctsync_printEventStartTime($event) {
        $startdate = new DateTime($event->startdate);
        $return = '<time datetime="'.$startdate->format('Y-m-d H:i:00').'">';
        $return .= $startdate->format('d.m.Y H:i');
        $return .= '</time>';
        return $return;
    }
}
if(!function_exists('ctsync_printEventEndTime')) {
    function ctsync_printEventEndTime($event) {
        $enddate = new DateTime($event->enddate);
        $start = (new DateTime($event->startdate))->setTime(0, 0, 0);
        $end = (new DateTime($event->enddate))->setTime(0, 0, 0);
        $return = '<time datetime="'.$enddate->format('Y-m-d H:i:00').'">';
        if($start == $end)
            $return .= $enddate->format('H:i');
        else
            $return .= $enddate->format('d.m.Y H:i');;
        $return .='</time>';
        return $return;
    }
}
if(!function_exists('ctsync_printEventTime')) {
    function ctsync_printEventTime($event, $atts) {
        $dateStart = strtotime($event->startdate);
        $dateEnd = strtotime($event->enddate);
        #$interval = (int) $dateStart->diff($dateEnd)->format('%a');
        //$return .= '<div class="cdate">' . date('d.m.Y \u\m H:i',$time) . '</div>';
        //$return .= '<div class="cdate">(bis ca. ' . date('d.m.Y \u\m H:i',$etime) . ')</div>';
        return ctsync_printEventStartTime($event).' - '.ctsync_printEventEndTime($event);
    }
}
if(!function_exists('ctsync_printEventLink')) { //TODO popup
    function ctsync_printEventLink($event, $wrap_text) {
        $return = '';
        if(isset($event->link)) {
            $return .= '<a href="' .$event->link. '">';
            $return .= $wrap_text;
            $return .= '</a>';
        } else {
            $return = $wrap_text;
        }
        return $return;
    }
}
if(!function_exists('ctsync_printEventCategory')) {
    function ctsync_printEventCategory($event, $atts) {
        if($atts['show_category'])
            return '<span class="event_category_name">'.htmlspecialchars($event->category_name).'</span>';
        else
            return '';
    }
}
if(!function_exists('ctsync_printEventPopup')) {
    function ctsync_printEventPopup($event, $atts, $wrap_text) {
        if($atts['modal_id'] != 'churchtools_event_popup')
            return '<a data-toggle="modal" class="btn btn-primary btn-lg" href="#'.$atts['modal_id'].'">'.$wrap_text.'</a>';
        else
            return $wrap_text;
    }
}
if(!function_exists('ctsync_printEventShortDesc')) {
    function ctsync_printEventShortDesc($event, $atts) {
        $content = Strip_tags($event->notizen);
        if (strlen($content > 150))
        {
            $pos=strpos($content, ' ', 150);
            if($pos<150)
                return $content;
            else
                return substr($content,0,$pos).'...';
        }
        else
        {
            return $content;
        }
    }
}

if(!function_exists('ctsync_printCalendarEvent')) {
    function ctsync_printCalendarEvent($event, $atts) {
        $return = '';
        switch ($atts['style']) {
        case 'short':
            $return .= '<p class="event">';
            $return .= '<strong>'.ctsync_printEventPopup($event, $atts, htmlspecialchars($event->bezeichnung)).'</strong><br />';
            $return .= ctsync_printEventTime($event, $atts);
            $return .= '</p>';
            break;
        case 'block':
            $return .= '<div class="event">'.'<div class="wrapper">'.'<div class="title_category">';
            $return .= '<center><h3>'.ctsync_printEventPopup($event, $atts, htmlspecialchars($event->bezeichnung)).'</h3></center>';
            $return .= '<center>'.ctsync_printEventTime($event, $atts).'</center>';
            $return .= '</div>';
            $return .= '<center>'.ctsync_printEventCategory($event, $atts).'</center>';
            $return .= '</div>'.'</div>';
            break;
        case 'detail-block':
            $return .= '<div class="event">'.'<div class="wrapper">'.'<div class="title_category">';
            $return .= '<center><h3>'.htmlspecialchars($event->bezeichnung).'</h3></center>';
            $return .= '<center><h4>'.ctsync_printEventTime($event, $atts).'</h3></center>';
            $return .= '</div>';
            $return .= '<center>'.ctsync_printEventCategory($event, $atts).'</center>';
            $return .= '<center>'.ctsync_printEventShortDesc($event, $atts).'</center>';
            //$return .= '<center>'.ctsync_printEventLink($event, 'Mehr Infos').'</center>';
            $return .= '<center>'.ctsync_printEventPopup($event, $atts, 'Mehr Infos').'</center>';
            $return .= '</div>'.'</div>';
            break;
        default:
            $return .= '<li class="event">'.'<div class="wrapper">'.'<div class="title_category">';
            $return .= '<h3>'.ctsync_printEventPopup($event, $atts, htmlspecialchars($event->bezeichnung)).'</h3>';
            $return .= ctsync_printEventCategory($event, $atts);
            $return .= '</div>';
            $return .= ctsync_printEventTime($event, $atts);
            $return .= '</div>'.'</li>';
            break;
        }
        return $return;
    }
}
/*if(!function_exists('ctsync_printEventModal')) {
    function ctsync_printEventModal($event, $atts) {
        $return = '';
        $return .= '<div id="111" class="modal" tabindex="-1" role="message">'.'<div class="modal-dialog" role="document">'.'<div class="modal-content">';
        $return .=  '<div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button><h4 class="modal-title">'.$event->bezeichnung.'</h4></div>';
        $return .= '<div class="modal-body">'.$event->notizen.'</div>';
        $return .= '<div class="modal-footer">'.ctsync_printEventTime($event, $atts).'</div>'.'</div>'.'</div>'.'</div>';
        return $return;
    }
}*/


if(!function_exists('ctsync_iterateCalendarEvents')) {
    function ctsync_iterateCalendarEvents(&$output, $atts) {
        $return = '';
        $i=0;
        $valid_evt = false; //checking if event matches filter or list has gone out of events
        foreach($output as $event){
            if(in_array($event->category_id,$atts['categories']) && $atts['title_matches'] != '' ? strpos(strtolower($event->bezeichnung), strtolower($atts['title_matches'])) !== false : true){
                if($i < $atts['offset'])
                    $i++;
                else {
                    $valid_evt = true; //found valid event
                    $return .= ctsync_printCalendarEvent($event, $atts);
                }
            }
        }
        //if no matching event was found, print error message
        $return .= !$valid_evt ? "No matching events available" : '';
        return $return;
    }
}
if(!function_exists('ctsync_printCalendarEvents')) {
    function ctsync_printCalendarEvents(&$output,$atts){
        wp_enqueue_style('ctstyle');
        $return='';
        $return .= '<div class="ct-events">'."\n";
        if(!empty($output)){
            switch ($atts['style']) {
            case 'short':
                $return .= ctsync_iterateCalendarEvents($output, $atts);
                break;
            default:
                if(count($atts['categories'])==1){
                    $ulClass='single_calendar';
                }else{
                    $ulClass='multi_calendar';
                }
                $return .= '<ul class="'.$ulClass.'">'."\n";
                $return .= ctsync_iterateCalendarEvents($output, $atts);
                $return .= '</ul>'."\n";
                break;
            }
        }else{
            $return.='<p>ChurchTools Fehler! Daten stehen momentan nicht zur Verfügung.</p>'."\n";
        }
        if($atts['show_update_info'])
            $return .= '<div class="last_updated"><strong>Last updated:</strong> ' . get_transient('churchtools_calendar_lastupdated').'</div>';
        $return .= '</div>'."\n";
        return $return;
    }
}

if(!function_exists('ctsync_printSingleEvent')) {
    function ctsync_printSingleEvent(&$events, $atts, $content) {
        $i=0;
        $valid_evt = false; //checking if event matches filter or list has gone out of events
        foreach($events as $event){
            if(in_array($event->category_id,$atts['categories']) && $atts['title_matches'] != '' ? strpos(strtolower($event->bezeichnung), strtolower($atts['title_matches'])) !== false : true){
                if($i < $atts['offset'])
                    $i++;
                else {
                    $valid_evt = true; //loop ends because of valid event
                    break;
                }
            }
        }
        //if loop ended because of to few events, return content of shortcode or error message
        if(!$valid_evt)
            return $content == '' ? "No matching event available" : $content;

        switch ($atts['component']) {
        case 'title':
            $return .= htmlspecialchars($event->bezeichnung);
            break;
        case 'description':
            $return .= htmlspecialchars($event->notizen);
            break;
        case 'category':
            $return .= ctsync_printEventCategory($event, $atts);
            break;
        case 'link':
            $return .= ctsync_printEventLink($event, $content);
            break;
        case 'startdate':
            $return .= ctsync_printEventStartTime($event);
            break;
        case 'endate':
            $return .= ctsync_printEventEndTime($event);
            break;
        case 'date':
            $return .= ctsync_printEventTime($event, $atts);
            break;
        case 'modal':
            $return .= '<div id="'.$atts['modal_id'].'" class="modal" tabindex="-1" role="message">'.'<div class="modal-dialog" role="document">'.'<div class="modal-content">';
            $return .= '<div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button><h4 class="modal-title">'.$event->bezeichnung.'</h4></div>';
            $return .= '<div class="modal-body"><p>'.$event->notizen.'</p>';
            $return .= '<p>'.ctsync_printEventLink($event, 'Mehr Infos').'</p></div>';
            $return .= '<div class="modal-footer">'.ctsync_printEventTime($event, $atts).'</div>'.'</div>'.'</div>'.'</div>';
            break;
        case 'event':
        default:
            $return .= ctsync_printCalendarEvent($event, $atts);
            break;
        }
        return $return;
    }
}
?>

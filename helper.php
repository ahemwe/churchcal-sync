<?php

const CT_URL = 'index.php?q=churchcal/ajax';
const CT_LOGIN_URL = 'index.php?q=login/ajax';
const CT_CSRF_URL = 'api/csrftoken';


$cookies = array();

if ( ! function_exists( 'ctsync_getCookies' ) ) {
	function ctsync_getCookies($cookies) {
	$res = "";
	foreach ($cookies as $key => $cookie) {
		$res .= "$key=$cookie; ";
	}
	return $res;
	}
}

if ( ! function_exists( 'ctsync_parseCookies' ) ) {
	function ctsync_parseCookies(&$r) {
	$cookies= array();
	foreach ($r as $hdr) {
		if (preg_match('/^Set-Cookie:\s*([^;]+)/i', $hdr, $matches)) {
            parse_str($matches[1], $tmp);
    		$cookies+= $tmp;
		}
	}
    return $cookies;
	}
}

if ( ! function_exists( 'ctsync_sendRequest' ) ) {
	function ctsync_sendRequest($url, $userid, $logintoken, $data) {
        $requestCookies= null;
        if ($userid != null && $logintoken != null)
        {
            // Now use token to login 
            $loginData = array('func' => 'loginWithToken', 
                'token' => $logintoken,
                'id' => $userid
            );
            $loginOptions = array(
                'http'=>array(
                'header' => "nContent-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($loginData),
                )
            );
            $loginContext = stream_context_create($loginOptions);
            $loginResult = file_get_contents($url.CT_LOGIN_URL, false, $loginContext);
            $loginObj = json_decode($loginResult);

            if ($loginObj->status == 'error') {
                echo "There is an error: $obj->message";
                //exit;
                return null;
            }
            $requestCookies= ctsync_parseCookies($http_response_header);

			// CSRF
            $header= "Content-type: application/x-www-form-urlencoded\r\n";
            $header= "Cookie: " . ctsync_getCookies($requestCookies) . "\r\n";
            $csrfOptions = array(
                'http'=>array(
                'header' => $header,
                'method' => 'GET'
                )
            );
            $csrfContext = stream_context_create($csrfOptions);
            $csrfResult = file_get_contents($url.CT_CSRF_URL, false, $csrfContext);
            $csrfobj = json_decode($csrfResult);
            if ($csrfobj->status == 'error') {
                echo "There is an error: $csrfobj->message";
                //exit;
                return null;
            }
            $csrfToken =$csrfobj->{'data'};
        }
        $header= "Content-type: application/x-www-form-urlencoded\r\n";
        if ($requestCookies != null)
        {
            $header.= "Cookie: " . ctsync_getCookies($requestCookies) . "\r\n";
        }
        if ($csrfToken != null)
        {
            $header.= "CSRF-Token: " . $csrfToken . "\r\n";
        }
        $options = array(
            'http'=>array(
            'header' => $header,
            'method' => 'POST',
            'content' => http_build_query($data),
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url.CT_URL, false, $context);
        $obj = json_decode($result);
	  
        if ($obj->status == 'error') {
            echo "There is an error: $obj->message";
            //exit;
            return null;
        }
        #ctsync_saveCookies($http_response_header);
        return $obj;
	}
}
?>